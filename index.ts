import type { RootContent } from "hast";

const unCapitablize = (string: string) =>
  `${string[0].toLowerCase()}${string.slice(1)}`;

const escapeAttributeName = (attribute: string) =>
  attribute === "className"
    ? "class"
    : attribute.startsWith("data")
    ? `data-${unCapitablize(attribute.slice(4))}`
    : attribute.startsWith("aria")
    ? `aria-${unCapitablize(attribute.slice(4))}`
    : attribute;

const escapeAttributeValue = (value: string | number) =>
  typeof value === "number" ? value : value.replaceAll('"', "&quot;");

const hastToHtml = (hast: RootContent | RootContent[]): string => {
  const stringify = (node: RootContent): string => {
    if (node.type === "doctype") {
      return "";
    }
    if (node.type === "text" || node.type === "comment") {
      return node.value;
    }
    const attributes = Object.entries(node.properties)
      .map(([attribute, value]) =>
        value == null
          ? ""
          : typeof value === "boolean"
          ? escapeAttributeName(attribute)
          : `${escapeAttributeName(attribute)}="${
              Array.isArray(value)
                ? value.map(escapeAttributeValue).join(" ")
                : escapeAttributeValue(value)
            }"`,
      )
      .join(" ");
    const tag =
      attributes === "" ? node.tagName : `${node.tagName} ${attributes}`;
    return node.children.length === 0
      ? `<${tag} />`
      : `<${tag}>${node.children.map(stringify).join("")}</${node.tagName}>`;
  };

  if (Array.isArray(hast)) {
    return hast.map(hastToHtml).join("");
  }

  switch (hast.type) {
    case "element":
      return stringify(hast);
    case "text":
      return hast.value;
    case "comment":
      return "";
    case "doctype":
      return "";
  }
};

export const jsxToHtml = (node: JSX.Element) => hastToHtml(node as any);
