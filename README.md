# jsx-to-html

## Install

```sh
npm i render-jsx-to-html
```
or
```sh
bun add render-jsx-to-html
```
and
```jsonc
// tsconfig.json
{
    "jsx": "react-jsx",
    "jsxImportSource": "render-jsx-to-html"
}
```

## Usage

```tsx
import { jsxToHtml } from "render-jsx-to-html";

function Hello({children}: JSX.Element) {
  return <div class="foo">Hello {children}</div>;
}

console.log(jsxToHtml(<Hello>World</Hello>)); // <div class="foo">Hello World</div>
```

## Why?

I wanted to explore HTMX with jsx as seen in [the BETH stack](https://www.youtube.com/watch?v=cpzowDDJj24&t=10s) but didn't want to install a dependency that's been unmaintained for 2 years while re-implementing things typescript already does.

## Approach

Utilize [typescript](https://www.typescriptlang.org/tsconfig#jsxImportSource) to transform JSX with [hastscript](https://www.npmjs.com/package/hastscript) and stringify the resulting tree.

## JSX

Oddly enough I couldn't find "plain" JSX types, so I utilized typescripts dom typings and created a small generic abstraction to better fit into JSX.

## Contributing

To install dependencies:

```bash
bun install
```

This project was created using `bun init` in bun v0.6.3. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
