import { expect, test } from "bun:test";
import { jsxToHtml } from ".";

test("plain html", () => {
  expect(jsxToHtml(<div>Hello World</div>)).toEqual("<div>Hello World</div>");
});

test("attributes", () => {
  expect(jsxToHtml(<div id="test">Test</div>)).toEqual(
    `<div id="test">Test</div>`,
  );
});

test("custom components", () => {
  const Hello = () => {
    return <>Test</>;
  };

  expect(jsxToHtml(<Hello />)).toEqual(`Test`);
});

test("fragment with multiple children", () => {
  expect(
    jsxToHtml(
      <>
        <span>Foo</span>
        <span>Bar</span>
      </>,
    ),
  ).toEqual(`<span>Foo</span><span>Bar</span>`);
});

test("data attributes", () => {
  expect(jsxToHtml(<span data-foo="foo" />)).toEqual(`<span data-foo="foo" />`);
});

test("attributes with quotes", () => {
  expect(
    jsxToHtml(<span hx-vals={JSON.stringify({ page: 1 })}>Test</span>),
  ).toEqual(`<span hx-vals="{&quot;page&quot;:1}">Test</span>`);
});

test("namespaced components", () => {
  const Foo = {
    Bar: () => <>Test</>,
  };
  expect(jsxToHtml(<Foo.Bar />)).toEqual("Test");
});

test("multiple attributes without values", () => {
  expect(jsxToHtml(<div aria-busy aria-disabled></div>)).toEqual(
    `<div aria-busy aria-disabled />`,
  );
});
