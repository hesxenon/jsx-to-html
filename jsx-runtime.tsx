/// <reference lib="dom" />
import { h } from "hastscript";
import type * as Hast from "hast";

declare global {
  export namespace JSX {
    export type Element<E extends HTMLElement = HTMLElement> = Partial<
      Omit<E, "children" | "className">
    > & {
      class?: string;
      children?: string | boolean | undefined | null | Element | Element[];
    };

    export type HTMLElements = {
      a: Element<HTMLAnchorElement>;
      abbr: Element<HTMLElement>;
      address: Element<HTMLElement>;
      area: Element<HTMLAreaElement>;
      article: Element<HTMLElement>;
      aside: Element<HTMLElement>;
      audio: Element<HTMLAudioElement>;
      b: Element<HTMLElement>;
      base: Element<HTMLBaseElement>;
      bdi: Element<HTMLElement>;
      bdo: Element<HTMLElement>;
      blockquote: Element<HTMLElement>;
      body: Element<HTMLBodyElement>;
      br: Element<HTMLBRElement>;
      button: Element<HTMLButtonElement>;
      canvas: Element<HTMLCanvasElement>;
      caption: Element<HTMLElement>;
      cite: Element<HTMLElement>;
      code: Element<HTMLElement>;
      col: Element<HTMLTableColElement>;
      colgroup: Element<HTMLTableColElement>;
      data: Element<HTMLElement>;
      datalist: Element<HTMLDataListElement>;
      dd: Element<HTMLElement>;
      del: Element<HTMLElement>;
      details: Element<HTMLDetailsElement>;
      dfn: Element<HTMLElement>;
      dialog: Element<HTMLDialogElement>;
      div: Element<HTMLDivElement>;
      dl: Element<HTMLDListElement>;
      dt: Element<HTMLElement>;
      em: Element<HTMLElement>;
      embed: Element<HTMLEmbedElement>;
      fieldset: Element<HTMLFieldSetElement>;
      figcaption: Element<HTMLElement>;
      figure: Element<HTMLElement>;
      footer: Element<HTMLElement>;
      form: Element<HTMLFormElement>;
      h1: Element<HTMLHeadingElement>;
      h2: Element<HTMLHeadingElement>;
      h3: Element<HTMLHeadingElement>;
      h4: Element<HTMLHeadingElement>;
      h5: Element<HTMLHeadingElement>;
      h6: Element<HTMLHeadingElement>;
      head: Element<HTMLHeadElement>;
      header: Element<HTMLElement>;
      hgroup: Element<HTMLElement>;
      hr: Element<HTMLHRElement>;
      html: Element<HTMLHtmlElement>;
      i: Element<HTMLElement>;
      iframe: Element<HTMLIFrameElement>;
      img: Element<HTMLImageElement>;
      input: Element<HTMLInputElement>;
      ins: Element<HTMLModElement>;
      kbd: Element<HTMLElement>;
      label: Element<HTMLLabelElement>;
      legend: Element<HTMLLegendElement>;
      li: Element<HTMLLIElement>;
      link: Element<HTMLLinkElement>;
      main: Element<HTMLElement>;
      map: Element<HTMLMapElement>;
      mark: Element<HTMLElement>;
      menu: Element<HTMLElement>;
      meta: Element<HTMLMetaElement>;
      meter: Element<HTMLElement>;
      nav: Element<HTMLElement>;
      noscript: Element<HTMLElement>;
      object: Element<HTMLObjectElement>;
      ol: Element<HTMLOListElement>;
      optgroup: Element<HTMLOptGroupElement>;
      option: Element<HTMLOptionElement>;
      output: Element<HTMLElement>;
      p: Element<HTMLParagraphElement>;
      picture: Element<HTMLElement>;
      pre: Element<HTMLPreElement>;
      progress: Element<HTMLProgressElement>;
      q: Element<HTMLQuoteElement>;
      rp: Element<HTMLElement>;
      rt: Element<HTMLElement>;
      ruby: Element<HTMLElement>;
      s: Element<HTMLElement>;
      samp: Element<HTMLElement>;
      script: Element<HTMLScriptElement>;
      section: Element<HTMLElement>;
      select: Element<HTMLSelectElement>;
      slot: Element;
      small: Element<HTMLElement>;
      source: Element<HTMLSourceElement>;
      span: Element<HTMLSpanElement>;
      strong: Element<HTMLElement>;
      style: Element<HTMLStyleElement>;
      sub: Element<HTMLElement>;
      summary: Element<HTMLElement>;
      sup: Element<HTMLElement>;
      table: Element<HTMLTableElement>;
      tbody: Element<HTMLTableSectionElement>;
      td: Element<HTMLTableCellElement>;
      template: Element<HTMLTemplateElement>;
      textarea: Element<HTMLTextAreaElement>;
      tfoot: Element<HTMLTableSectionElement>;
      th: Element<HTMLTableCellElement>;
      thead: Element<HTMLTableSectionElement>;
      time: Element<HTMLElement>;
      title: Element<HTMLTitleElement>;
      tr: Element<HTMLTableRowElement>;
      track: Element<HTMLTrackElement>;
      u: Element<HTMLElement>;
      ul: Element<HTMLUListElement>;
      var: Element<HTMLElement>;
      video: Element<HTMLVideoElement>;
      wbr: Element<HTMLElement>;
    };

    export type IntrinsicElements = HTMLElements;
    type IntrinsicElement = IntrinsicElements[keyof IntrinsicElements];
  }
}

export function Fragment({ children }: { children: any }): Hast.ElementContent {
  return typeof children !== "object" && typeof children !== "function"
    ? { type: "text", value: children }
    : children;
}

export const jsx = (
  tag: any,
  { children, ...attributes }: Record<string, any>,
) => {
  return typeof tag === "function"
    ? tag({ children, ...attributes })
    : h(tag, attributes, children);
};
